import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';

import { IframewrapperPage } from '../iframewrapper/iframewrapper';

import { SitelocationPage } from '../sitelocation/sitelocation';

import {AngularFire, FirebaseListObservable} from 'angularfire2';

//import { AdMob } from 'ionic-native';

@Component({
  selector: 'page-universities',
  templateUrl: 'universities.html'
}) 

export class UniversidadesPage {

  universities:FirebaseListObservable<any[]>;
  //bannerSmallId:string='ca-app-pub-1515622676645708/4766907467';

  constructor(public navCtrl: NavController, af: AngularFire, public loadingCtrl: LoadingController) {
       
       let loader = this.loadingCtrl.create({
          content: "Cargando datos..."      
       });
       
       loader.present();


      this.universities=af.database.list('/universities');

      this.universities.subscribe(data=>{
        console.log('llego data:'+data);
        loader.dismiss();
      });
      
 
  }

  launch(url,title){ 
    this.navCtrl.push(IframewrapperPage,{url:url,title:title});
     // this.platform.ready().then(() => {
        //(<any>window).plugins.cordova.InAppBrowser.open(url, "_system", "location=true");
            //cordova.InAppBrowser.open(url, "_system", "location=true");
       //   window.open(url, "_system", "location=true");
      //});
  }
  pinMap(univ){
    console.log('sending:',univ);
    this.navCtrl.push(SitelocationPage,univ);
  }

 /* showBannerSmall(){
       this.platform.ready().then(() => {
            if(AdMob) {
                AdMob.createBanner(this.bannerSmallId);
            }
        });
  }*/

}
