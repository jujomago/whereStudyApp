import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { IframewrapperPage } from '../iframewrapper/iframewrapper'; 

import {AngularFire, FirebaseListObservable} from 'angularfire2';

@Component({
  selector: 'page-institutos',
  templateUrl: 'institutos.html'
})
export class InstitutosPage {
    institutos:FirebaseListObservable<any[]>;

  constructor(public navCtrl: NavController, af: AngularFire, public loadingCtrl: LoadingController) {
       let loader = this.loadingCtrl.create({
          content: "Cargando datos..."      
       });
       
       loader.present();
        this.institutos=af.database.list('/institutes');
        this.institutos.subscribe(data=>{
        console.log('llego data:'+data);
        loader.dismiss();
      });
  }

    launch(url,title){ 
    this.navCtrl.push(IframewrapperPage,{url:url,title:title});   
  }
}
