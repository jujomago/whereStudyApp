import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  mylist:any[]=[];

  constructor(public navCtrl: NavController) {
    this.mylist.push('hola')
    this.mylist.push('que')
    this.mylist.push('tal');
  }

}
