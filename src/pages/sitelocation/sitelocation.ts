import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';

declare var google;
/*
  Generated class for the Sitelocation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sitelocation',
  templateUrl: 'sitelocation.html'
})
export class SitelocationPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  textPin: String;
  coordPin: String;
  lugar:String;

  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation) {
   console.log(navParams.get('fullName'));
    this.textPin = navParams.get('fullName');
    this.coordPin = navParams.get('coordinates');
    this.lugar=navParams.get('sigla');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SitelocationPage');
    this.loadMap();
  }
  loadMap() {


    this.geolocation.getCurrentPosition()
      .then(position => {

        console.log('the position:', position);


        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
       // return this.map;
      })
      .then(() => {
        let theLat = this.coordPin.split(',')[0];
        let theLong = this.coordPin.split(',')[1];
        let latLng = new google.maps.LatLng(theLat, theLong);
        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng,
          title: 'Hello World!'
        });
        this.map.setCenter(latLng);
        let content = "<h4>" + this.textPin + "</h4>";
        this.addInfoWindow(marker, content);
      })

      .catch(err => { console.log(err); });

  }

  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }

}
