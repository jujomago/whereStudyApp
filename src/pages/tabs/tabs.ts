import { Component } from '@angular/core';

import { UniversidadesPage } from '../universities/universities';
import { InstitutosPage } from '../institutos/institutos';
import { ContactPage } from '../contact/contact';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = UniversidadesPage;
  tab2Root: any = InstitutosPage;
  tab3Root: any = ContactPage;

  constructor() {

  }
}
