import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';


/*
  Generated class for the Iframewrapper page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-iframewrapper',
  templateUrl: 'iframewrapper.html'
})
export class IframewrapperPage {
  theUrl:String;
  title:String;
  constructor(private sanitizer: DomSanitizer, public navCtrl: NavController, public navParams: NavParams) {
   
    this.theUrl=navParams.get('url');
    this.title=navParams.get('title');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IframewrapperPage');
  }

}
