import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { UniversidadesPage } from '../pages/universities/universities';
import { InstitutosPage } from '../pages/institutos/institutos';
import { ContactPage } from '../pages/contact/contact';
import { IframewrapperPage } from '../pages/iframewrapper/iframewrapper';
import { SitelocationPage } from '../pages/sitelocation/sitelocation';

import { AngularFireModule } from 'angularfire2';
import { Geolocation } from '@ionic-native/geolocation';

// AF2 Settings
export const firebaseConfig = {
  apiKey: "AIzaSyBXQXhkG5t93KtI7UWr6-BSzHg15g1yuPE",
  authDomain: "intense-torch-4347.firebaseapp.com",
  databaseURL: "https://intense-torch-4347.firebaseio.com",
  storageBucket: "intense-torch-4347.appspot.com",
  messagingSenderId: "753006132889"
};


@NgModule({
  declarations: [
    MyApp,
    InstitutosPage,
    ContactPage,
    UniversidadesPage,
    TabsPage,
    IframewrapperPage,
    SitelocationPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    InstitutosPage,
    ContactPage,
    UniversidadesPage,
    TabsPage,
    IframewrapperPage,
    SitelocationPage
  ],
  providers: [Geolocation,{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
